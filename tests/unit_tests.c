/**
 * @brief      This file implements unit tests for sorting algorithms
 *
 * @author     Jeremie
 * @date       2022
 */
#include <check.h>
#include <sort_algorithms.h>
#include <stdlib.h>
#include <utils.h>

/**
 * @brief Simple test that checks whether the random number generator has
 *        has been initialized
 */
START_TEST (test_rn) { ck_assert_int_eq (init_rn_generator (), 1); }
END_TEST


START_TEST (test_array)
{
    // déclaration et initialisation de variables
    int N=10;
    int V_MAX=10;
    int t[N];
    // appel de la fonction à tester
    init_array (t,N,V_MAX);
    // test du résultat de la fonction avec ck_assert
    for(int i=0;i<N;i++){
        // on verifie que t[i] est superieur a 0
        ck_assert_int_ge (t[i], 0);
        //on verifie que t[i] est inferieur a V_Max
        ck_assert_int_lt (t[i], V_MAX);
    }
}
END_TEST

START_TEST (test_swap_value_in_array)
{
    int N = 5;
    int t[N];
    unsigned int i = 1; 
    unsigned int j = 3; 
    
    t[0] = 1; t[1] = 2; t[2] = 3; t[3] = 4; t[4] = 5;
    
    int expected[N];
    expected[0] = 1; expected[1] = 4; expected[2] = 3; expected[3] = 2; expected[4] = 5;
    
    swap_value_in_array(t, i, j);
    
    for(int idx = 0; idx < N; idx++){
        ck_assert_int_eq(t[idx], expected[idx]);
    }
}
END_TEST

START_TEST (test_selection_sort)
{
    // Initialize variables
    int N = 5;
    int t[N];
    t[0] = 4; t[1] = 1; t[2] = 3; t[3] = 5; t[4] = 2;
    
    // Expected sorted array
    int expected[N];
    expected[0] = 1; expected[1] = 2; expected[2] = 3; expected[3] = 4; expected[4] = 5;
    
    // Call the function to test
    selection_sort(t, N);
    
    // Test if the array is sorted
    for(int idx = 0; idx < N; idx++){
        ck_assert_int_eq(t[idx], expected[idx]);
    }
}
END_TEST

START_TEST (test_insertion_sort)
{
    // Initialize variables
    int N = 5;
    int t[N];
    t[0] = 4; t[1] = 1; t[2] = 3; t[3] = 5; t[4] = 2;
    
    // Expected sorted array
    int expected[N];
    expected[0] = 1; expected[1] = 2; expected[2] = 3; expected[3] = 4; expected[4] = 5;
    
    // Call the function to test
    insertion_sort(t, N);
    
    // Test if the array is sorted
    for(int idx = 0; idx < N; idx++){
        ck_assert_int_eq(t[idx], expected[idx]);
    }
}
END_TEST

START_TEST (test_merge)
{
    int t[] = {1, 4, 5, 2, 3, 6}; 
    unsigned int start_sub_tab_1 = 0;
    int last_sub_tab_1 = 2; 
    int last_sub_tab_2 = 5; 
    
    int expected[] = {1, 2, 3, 4, 5, 6};
    
    merge(t, start_sub_tab_1, last_sub_tab_1, last_sub_tab_2);
    
    int N = sizeof(t) / sizeof(t[0]); 
    for(int idx = 0; idx < N; idx++){
        ck_assert_int_eq(t[idx], expected[idx]);
    }
}
END_TEST

START_TEST (test_recursive_merge)
{
    int t[] = {3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5};
    unsigned int first = 0;
    unsigned int last = sizeof(t) / sizeof(t[0]) - 1;
    
    int expected[] = {1, 1, 2, 3, 3, 4, 5, 5, 5, 6, 9};
    
    recursive_merge(t, first, last);
    
    int N = sizeof(t) / sizeof(t[0]);
    for(int idx = 0; idx < N; idx++){
        ck_assert_int_eq(t[idx], expected[idx]);
    }
}
END_TEST

START_TEST (test_merge_sort)
{
    int t[] = {8, 4, 3, 7, 6, 5, 2, 1};
    int N = sizeof(t) / sizeof(t[0]); 
    
    int expected[] = {1, 2, 3, 4, 5, 6, 7, 8};
    
    merge_sort(t, N);
    
    for(int idx = 0; idx < N; idx++){
        ck_assert_int_eq(t[idx], expected[idx]);
    }
}
END_TEST


Suite *sort_suite (void)
{
    Suite *s       = suite_create ("SortAlgorithms");
    TCase *tc_core = tcase_create ("Core");
    tcase_add_test (tc_core, test_rn);
    
    tcase_add_test (tc_core, test_array);
    
    tcase_add_test (tc_core, test_swap_value_in_array);
    
    tcase_add_test (tc_core, test_selection_sort);
    
    tcase_add_test (tc_core,test_insertion_sort);
    
    tcase_add_test (tc_core,test_merge);
    
    tcase_add_test (tc_core,test_recursive_merge);
    
    tcase_add_test (tc_core,test_merge_sort);

    
    suite_add_tcase (s, tc_core);
    
    return s;
}





int main (void)
{
    int      no_failed = 0;
    Suite   *s         = sort_suite ();
    SRunner *runner    = srunner_create (s);
    srunner_run_all (runner, CK_NORMAL);
    no_failed = srunner_ntests_failed (runner);
    srunner_free (runner);
    return (no_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
